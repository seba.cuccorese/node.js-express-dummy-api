import joi from "joi"

export const validate = (name) => {
    const schema = joi.object({
        name: joi.string().min(3).required()
    });

    return schema.validate({ name: name });
}