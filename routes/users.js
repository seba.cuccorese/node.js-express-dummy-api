import  Express  from 'express';
import { validate } from '../validations.js';
import { findUser } from '../userFunction.js';

const router = Express.Router();


const usuarios = [
    { id: 1, name: 'Sebastian' },
    { id: 2, name: 'Julien' },
    { id: 3, name: 'Batistuta' },
    { id: 4, name: 'Pino' },
    { id: 5, name: 'Papafrita' }
];


router.get('/',
    (req, res) => res.send(usuarios));

router.get('/:id',
    (req, res) => {
        let user = findUser(usuarios, req.params.id);
        user ? res.status(200).send(user) : res.status(404).send('User not found');
    })

router.post('/', (req, res) => {
    const error = validate(req.body.name).error
    if (error) {
        res.status(400).send(error.message);
        return;
    }
    const usuario = {
        id: usuarios.length + 1,
        name: req.body.name
    }
    usuarios.push(usuario);
    res.status(201).send({ id: usuario.id });
});


router.put('/:id', (req, res) => {
    let user = findUser(usuarios, req.params.id)
    if (!user) {
        res.status(404).send('User not found');
        return;
    }
    const error = validate(req.body.name).error;
    if (error) {
        res.status(400).send(error.message);
        return;
    }

    user.name = req.body.name;

    res.status(200).send(user);
});


router.delete('/:id', (req, res) => {
    let user = findUser(usuarios, req.params.id);
    if (!user) {
        res.status(404).send('User not found');
        return;
    }
    const index = usuarios.indexOf(user);
    usuarios.splice(index, 1);
    res.status(204).send();
})

export default router;