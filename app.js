import express from 'express';
import morgan from 'morgan';
import config from 'config';
import { log } from './logger.js';
import routes from './routes/users.js';
import debug from 'debug';

const startDebug = debug('app:start');
const dbDebug = debug('db:connection');
const app = express();

app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(express.static('public'));
app.use('/api/usuarios', routes);

//CONFIG DE ENTORNOS
//SET NODE_ENV=production
console.log(`Application name: ${config.get('name')}`);
console.log(`DB SERVER: ${config.get('configDB.host')}`);
dbDebug('db connection started...')

//Middleware de 3ero
if (app.get('env') === 'development') {
    app.use(morgan('tiny'));
    startDebug('Morgan started...')
}

app.use(log);

app.use((req, res, next) => {
    console.log('Checking user...');
    next();
});

app.get('/',
    (req, res) => res.send('Hola mundo'));

const port = process.env.PORT || 3000;

app.listen(port,
    () => console.log(`listening on port: ${port}...`));


