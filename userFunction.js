import util from 'util';

export const findUser = (users, id) => {
    return users.find((user) =>
        util.isDeepStrictEqual(user.id, parseInt(id)))
}